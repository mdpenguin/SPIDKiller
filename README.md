# SPID Killer

SPID Killer is an Electron-based tool to view and kill active server processes on SQL Server.

It is designed for cases where the server load is so high that the overhead of connecting with SSMS or another IDE makes it difficult to connect and the user is not familiar with sqlcmd.