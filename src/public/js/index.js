import createList from './list.js';
import createMessage from './message.js';
import spinner from './spinner.js';
const winAuthChk = document.getElementById('win_auth');
const loginFields = document.querySelectorAll('.login');
const connectBtn = document.getElementById('connect_btn');
const disconnectBtn = document.getElementById('disconnect_btn');
const killBtn = document.getElementById('kill_btn');
const connectForm = document.getElementById('connect');
const killForm = document.getElementById('kill');
const listBtn = document.getElementById('list_btn');

// disable user name and password fields if using windows authentication
winAuthChk.addEventListener('change', function () {
    loginFields.forEach(field => {
        const disabled = this.checked ? true : false;
        field.disabled = disabled;
        field.required = !disabled;
        field.value = '';
    });
});

// handle form submit
document.addEventListener('submit', async function (e) {
    e.preventDefault();

    let results;

    try {
        spinner.renderSpinner();

        const type = e.submitter.id.replace('_btn', '');

        createMessage.clear();

        // get form data
        const data = type === 'connect' ? new FormData(connectForm, connectBtn) : new FormData(killForm, killBtn);

        const paramObj = {};
        data.forEach((value, key) => {
            paramObj[key
                .replace('win_auth', 'winAuth')
                .replace('conn_timeout', 'connectionTimeout')
                .replace('req_timeout', 'requestTimeout')] = value;
        });

        // process form request
        if (type === 'kill') {
            results = await window.electronAPI.killSPID(Number(paramObj.spid));

            const row = document.getElementById(`spid${paramObj.spid}`);
            const classesToRemove = ['even', 'odd'];
            row.classList.remove(...classesToRemove);
            row.classList.add('dead');
        }
        else if (type === 'connect') {
            connectBtn.disabled = true;

            results = await window.electronAPI.connectPool(paramObj);

            disconnectBtn.disabled = false;
            killBtn.disabled = false;
            listBtn.disabled = false;
        }
        else if (type === 'disconnect') {
            disconnectBtn.disabled = true;
            connectBtn.disabled = false;
            killBtn.disabled = true;
            listBtn.disabled = true;

            createList.clear();

            results = await window.electronAPI.closePool();
        }

        spinner.clear();

        if (results.status === 'failed') throw results.message;
    } catch (err) {
        createMessage.render(err ? err : 'An error has occurred', true);
    }
});

// handle list server processes
listBtn.addEventListener('click', async function () {
    createMessage.clear();
    spinner.renderSpinner();

    const data = await window.electronAPI.getProcData();

    createList.render(data.recordsets);

    spinner.clear();
});