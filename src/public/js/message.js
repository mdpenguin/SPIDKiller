class CreateMessage {
    _parentElement = document.querySelector('.message');

    clear() {
        this._parentElement.innerHTML = "";
    }

    render(message, isError = false) {
        this.clear();

        this._parentElement.insertAdjacentHTML('beforeend', this._markup(message, isError));
    }

    _markup(message, isError) {
        return `<span ${isError ? 'style="color: red; background-color: lightgray"><strong' : ''}>${message}</${isError ? 'strong></' : ''}span>`;
    }
}

export default new CreateMessage();