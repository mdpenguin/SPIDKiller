class CreateList {
    _parentElement = document.querySelector('.list');

    clear() {
        this._parentElement.innerHTML = "";
    }

    _sanitizeHTML = (data) => {
        let cleanData;

        if (typeof data === 'string') {
            cleanData = data.replace(/&/g, '&amp;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;')
                .replace(/"/g, '&quot;')
                .replace(/'/g, '&#x27;');
        }
        else {
            cleanData = data;
        }

        return cleanData;
    };

    _generateRow(rowData, i) {
        return `
        <tr id="spid${rowData.SPID}" class="${i % 2 === 1 ? 'even' : 'odd'}">
            <td>${this._sanitizeHTML(rowData.SPID)}</td>
            <td>${this._sanitizeHTML(rowData.Database)}</td>
            <td>${this._sanitizeHTML(rowData.StartTime)}</td>
            <td>${this._sanitizeHTML(rowData.TotalElapsedTime)}</td>
            <td>${this._sanitizeHTML(rowData.Command)}</td>
            <td class="big">${this._sanitizeHTML(rowData.QueryText).trim()}</td>
            <td>${this._sanitizeHTML(rowData.PctComplete)}</td>
            <td>${this._sanitizeHTML(rowData.Status)}</td>
            <td>${this._sanitizeHTML(rowData.BlockedBySPID)}</td>
            <td>${this._sanitizeHTML(rowData.CPUTime)}</td>
            <td>${this._sanitizeHTML(rowData.GrantedMemory)}</td>
            <td>${this._sanitizeHTML(rowData.PhysicalIO)}</td>
            <td>${this._sanitizeHTML(rowData.DoP)}</td>
            <td>${this._sanitizeHTML(rowData.WaitTime)}</td>
            <td>${this._sanitizeHTML(rowData.LastWaitType)}</td>
            <td class="big">${this._sanitizeHTML(rowData.LoginName).trim()}</td>
            <td>${this._sanitizeHTML(rowData.HostName)}</td>
            <td class="big">${this._sanitizeHTML(rowData.ProgramName).trim()}</td>
        </tr>
        `;
    }

    _generateTable(data) {
        return `
    <table>
        <thead>
            <tr>
                <th>SPID</th>
                <th>Database</th>
                <th>Start<br/>Time</th>
                <th>Total<br/>Elapsed<br/>Time</th>
                <th>Command</th>
                <th style="min-width: 250px">Query<br />Text</th>
                <th>Pct<br/>Complete</th>
                <th>Status</th>
                <th>Blocked<br/>By<br/>SPID</th>
                <th>CPU<br/>Time</th>
                <th>Granted<br/>Memory</th>
                <th>Physical<br/>IO</th>
                <th>DoP</th>
                <th>Wait<br/>Time</th>
                <th>Last<br/>Wait<br/>Type</th>
                <th>Login<br/>Name</th>
                <th>Host<br/>Name</th>
                <th style="max-width: 300px">Program<br/>Name</th>
            </tr>
        </thead>
        <tbody>
            ${data[0].map((rowData, i) => this._generateRow(rowData, i)).join('')}
        </tbody>
    </table>
      `;
    }

    render(data) {
        if (!data || (Array.isArray(data) && data.length === 0)) return;

        const markup = this._generateTable(data);

        this.clear();
        this._parentElement.insertAdjacentHTML("afterbegin", markup);
    }
}

export default new CreateList();