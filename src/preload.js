const { contextBridge, ipcRenderer } = require('electron');

contextBridge.exposeInMainWorld('electronAPI', {
    connectPool: (connParams) => ipcRenderer.invoke('connect-pool', connParams),
    killSPID: (spid) => ipcRenderer.invoke('kill-spid', spid),
    closePool: () => ipcRenderer.invoke('close-pool'),
    getProcData: () => ipcRenderer.invoke('get-proc-data')
});