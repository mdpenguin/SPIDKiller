const sql = require('mssql/msnodesqlv8');

let pool;

const createPool = function (poolParams) {
    const connectionConfig = {
        server: poolParams.serverName,
        database: 'master',
        driver: 'msnodesqlv8',
        connectionTimeout: poolParams.connectionTimeout,
        requestTimeout: poolParams.requestTimeout,
        options: {
            multiSubnetFailover: false
        }
    };

    if (poolParams.trustedConnection)
        connectionConfig.options.trustedConnection = poolParams.trustedConnection;
    else {
        connectionConfig.user = poolParams.user;
        connectionConfig.password = poolParams.password;
    }

    pool = new sql.ConnectionPool(connectionConfig);
};

const getProcDataQuery = `
SELECT p.spid                                     'SPID',
       db.name                                    'Database',
       FORMAT(r.start_time,'yyyy-MM-dd HH:mm:ss') 'StartTime',
       r.total_elapsed_time                       'TotalElapsedTime',
       r.command                                  'Command',
       t.TEXT                                     'QueryText',
       r.percent_complete                         'PctComplete',
       p.status                                   'Status',
       p.blocked                                  'BlockedBySPID',
       p.cpu                                      'CPUTime',
       r.granted_query_memory                     'GrantedMemory',
       p.physical_io                              'PhysicalIO',
       r.dop                                      'DoP',
       p.WaitTime                                 'WaitTime',
       p.LastWaitType                             'LastWaitType',
       p.loginame                                 'LoginName',
       p.hostname                                 'HostName',
       p.program_name                             'ProgramName'
FROM sys.sysProcesses                          p
JOIN sys.databases                             db ON p.dbid = db.database_id
JOIN sys.dm_exec_requests                      r ON p.spid = r.session_id
CROSS APPLY sys.dm_exec_sql_text(p.sql_handle) t
WHERE p.spid <> @@SPID
      AND p.status NOT IN ('background', 'sleeping')
      AND p.cmd NOT IN ('AWAITING COMMAND', 'MIRROR HANDLER', 'LAZY WRITER', 'CHECKPOINT SLEEP', 'RA MANAGER')
ORDER BY r.total_elapsed_time
`;

const connectPool = async function (connParams) {
    try {
        if (!pool) {
            const poolParams = {
                serverName: connParams.server,
                trustedConnection: connParams.winAuth ? true : false,
                connectionTimeout: connParams.connectionTimeout * 1000,
                requestTimeout: connParams.requestTimeout * 1000,
                user: connParams.user,
                password: connParams.password
            };

            createPool(poolParams);
        }

        if (!pool._connected) {
            await pool.connect();
        }

        return {
            status: 'success',
            message: 'Connected to server'
        };
    } catch (err) {
        return {
            status: 'failed',
            message: err
        };
    }
};

const getProcData = async function (query = getProcDataQuery) {
    try {
        await connectPool();
        const results = await pool.request()
            .query(query);

        return results;
    } catch (err) {
        return {
            status: 'failed',
            message: err
        };
    }
};

const killSPID = async function (SPID) {
    try {
        if (!Number.isInteger(SPID)) {
            throw new Error('Invalid SPID');
        };

        await connectPool();
        await pool.request()
            .input('stmt', sql.NVarChar, `kill ${Number(SPID)}`) /* casting as number since cannot use prepared statement for kill */
            .execute('SP_EXECUTESQL');

        return {
            status: 'success',
            message: 'Kill request sent to server'
        };
    } catch (err) {
        return {
            status: 'failed',
            message: err
        };
    }
};

const closePool = async function () {
    try {
        if (pool?._connected) {
            await pool.close();
        }

        pool = undefined;

        return {
            status: 'success',
            message: 'Connection has been closed'
        };
    } catch (err) {
        return {
            status: 'failed',
            message: err
        };
    }
};

module.exports = {
    connectPool,
    getProcData,
    killSPID,
    closePool
};