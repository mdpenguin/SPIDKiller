const { app, BrowserWindow, ipcMain } = require('electron');
const path = require('path');
const sql = require(__dirname + '/sql.js');

function main() {
    const winOptions = {
        width: 1280,
        height: 960,
        icon: __dirname + '/img/icon.ico',
        webPreferences: {
            preload: path.join(__dirname, 'preload.js')
        }
    };

    let win = new BrowserWindow(winOptions);

    win.removeMenu();

    win.loadFile(__dirname + '/public/index.html');

    win.on('close', () => {
        win = null;
    });
}

app.whenReady()
    .then(() => {
        ipcMain.handle('connect-pool', (event, connectParam) => sql.connectPool(connectParam));
        ipcMain.handle('close-pool', () => sql.closePool());
        ipcMain.handle('kill-spid', (event, spid) => sql.killSPID(spid));
        ipcMain.handle('get-proc-data', () => sql.getProcData());

        main();
    })

