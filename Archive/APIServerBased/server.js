const
    fs = require('fs'),
    https = require('https'),
    app = require(__dirname + '/app');

const
    port = 8086,
    options = {
        key: fs.readFileSync(__dirname + '/cert/key.pem'),
        cert: fs.readFileSync(__dirname + '/cert/cert.pem'),
        passphrase: 'IdealMiracleFabricateSermon'
    };

https.createServer(options, app)
    .listen(port);

module.exports = { port };