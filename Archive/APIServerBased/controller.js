const sql = require('./sql');

exports.checkSPID = function (req, res, next, val) {
    if (!Number(val))
        return res.status(400).json({
            status: 'fail',
            message: 'Invalid SPID'
        });
    next();
};

exports.checkConnParams = function (req, res, next) {
    if (!req.query.server || !(req.query.winAuth || (req.query.user && req.query.password)))
        return res.status(400).json({
            status: 'fail',
            message: 'Invalid connection parameters'
        });
    next();
};

exports.connect = async function (req, res, next) {
    sql.connectPool(req.query)
        .then(() => res.status(202).json({
            status: 'success',
            message: `Successfully connected to ${req.query.server}`
        }));
};

exports.fetchProcData = async function (req, res, next) {
    sql.getProcData()
        .then((data) => res.status(202).json({
            status: 'success',
            data: data
        }));
};

exports.kill = async function (req, res, next) {
    sql.killSPID(Number(req.params.spid))
        .then(data => {
            res.status(200).json({
                status: 'success',
                data: data
            });
        });
};

exports.close = async function (req, res, next) {
    sql.closePool()
        .then(() => {
            res.status(204).json({
                status: 'success',
                data: null
            });
        });
};

exports.exit = async function (req, res, next) {
    sql.closePool()
        .then(() => process.exit());
};