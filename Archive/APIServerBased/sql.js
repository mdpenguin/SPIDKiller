const sql = require('mssql/msnodesqlv8');

const connectionConfig = {
    database: 'master',
    driver: 'msnodesqlv8',
    connectionTimeout: 600_000,
    requestTimeout: 300_000,
    options: {
        multiSubnetFailover: false
    }
};

let pool;

const createPool = function (serverName, trustedConnection = true, user, password) {
    connectionConfig.server = serverName;

    if (trustedConnection)
        connectionConfig.options.trustedConnection = trustedConnection;
    else {
        connectionConfig.user = user;
        connectionConfig.password = password;
    }

    pool = new sql.ConnectionPool(connectionConfig);
};



const getProcDataQuery = `
SELECT p.spid                                     'SPID',
       db.name                                    'Database',
       FORMAT(r.start_time,'yyyy-MM-dd HH:mm:ss') 'StartTime',
       r.total_elapsed_time                       'TotalElapsedTime',
       r.command                                  'Command',
       t.TEXT                                     'QueryText',
       r.percent_complete                         'PctComplete',
       p.status                                   'Status',
       p.blocked                                  'BlockedBySPID',
       p.cpu                                      'CPUTime',
       r.granted_query_memory                     'GrantedMemory',
       p.physical_io                              'PhysicalIO',
       r.dop                                      'DoP',
       p.WaitTime                                 'WaitTime',
       p.LastWaitType                             'LastWaitType',
       p.loginame                                 'LoginName',
       p.hostname                                 'HostName',
       p.program_name                             'ProgramName'
FROM sys.sysProcesses                          p
JOIN sys.databases                             db ON p.dbid = db.database_id
JOIN sys.dm_exec_requests                      r ON p.spid = r.session_id
CROSS APPLY sys.dm_exec_sql_text(p.sql_handle) t
WHERE p.spid <> @@SPID
      AND p.status NOT IN ('background', 'sleeping')
      AND p.cmd NOT IN ('AWAITING COMMAND', 'MIRROR HANDLER', 'LAZY WRITER', 'CHECKPOINT SLEEP', 'RA MANAGER')
ORDER BY r.total_elapsed_time
`;

let isConnected = false;

const connectPool = async function (connParams) {
    if (!pool) {
        const
            serverName = connParams.server,
            trustedConnection = connParams.winAuth ? true : false,
            user = connParams.user,
            password = connParams.password;

        createPool(serverName, trustedConnection, user, password);
    }

    if (!pool._connected) {
        await pool
            .connect()
            .then(() => {
                isConnected = true;
            })
            .catch(err => {
                throw err;
            });
    }
    return {
        status: 'success',
        message: 'Pool connected'
    };
};

const getProcData = async function (query = getProcDataQuery) {
    await connectPool();
    const results = await pool.request()
        .query(query)
        .catch(err => {
            throw err;
        });

    return results;
};

const killSPID = async function (SPID) {
    try {
        if (!Number.isInteger(SPID)) {
            throw new Error('Invalid SPID');
        };

        await connectPool();
        await pool.request()
            .input('stmt', sql.NVarChar, `kill ${Number(SPID)}`) /* casting as number since cannot use prepared statement for kill */
            .execute('SP_EXECUTESQL');

        return {
            status: 'success',
            spid: SPID
        };
    } catch (err) {
        throw err;
    }
};

const closePool = async function () {
    if (pool) {
        pool.close()
            .then(() => {
                isConnected = false;
                return {
                    status: 'success',
                    message: 'Pool closed'
                };
            })
            .catch((err) => {
                throw err;
            });
    }
};


module.exports = {
    connectPool,
    getProcData,
    killSPID,
    closePool
};