const { app, BrowserWindow } = require('electron');
const server = require(__dirname + '/server.js');

function main() {
    const winOptions = {
        width: 1280,
        height: 960,
        icon: __dirname + '/img/icon.ico'
    };

    let win = new BrowserWindow(winOptions);

    win.removeMenu();

    win.loadURL(`https://127.0.0.1:${server.port}`);

    win.on('close', () => {
        win = null;
    });
}

app.commandLine.appendSwitch('ignore-certificate-errors');

app.on('ready', main);

