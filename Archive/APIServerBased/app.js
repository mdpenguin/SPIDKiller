const express = require('express');
const routes = require(__dirname + '/routes');

const app = express();

app.use(express.json());
app.use(express.static(__dirname + '/public'));

app.use('/api', routes);

module.exports = app;