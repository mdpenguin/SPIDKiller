const express = require('express');
const controller = require(__dirname + '/controller');

const router = express.Router();

router.param('spid', controller.checkSPID);

router.route('/connect')
    .post(controller.checkConnParams, controller.connect);

router.route('/disconnect')
    .post(controller.close);

router.route('/kill/:spid')
    .post(controller.kill);

router.route('/list')
    .get(controller.fetchProcData);

router.route('/exit')
    .post(controller.exit);

module.exports = router;