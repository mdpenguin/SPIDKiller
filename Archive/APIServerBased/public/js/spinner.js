class Spinner {
    _parentElement = document.querySelector('.modal');
    _spinner = `<div class="spinner"></div>`;

    clear() {
        this._parentElement.classList.add('hidden');
        this._parentElement.innerHTML = "";
    }

    renderSpinner() {
        this.clear();
        this._parentElement.insertAdjacentHTML("afterbegin", this._spinner);
        this._parentElement.classList.remove('hidden');
    }
}

export default new Spinner();