import createList from './list.js';
import spinner from './spinner.js';
const winAuthChk = document.getElementById('win_auth');
const loginFields = document.querySelectorAll('.login');
const connectBtn = document.getElementById('connect_btn');
const disconnectBtn = document.getElementById('disconnect_btn');
const killBtn = document.getElementById('kill_btn');
const connectForm = document.getElementById('connect');
const killForm = document.getElementById('kill');
const listBtn = document.getElementById('list_btn');
/* 
TODO:
Handle errors
Refactor...do I really care?
*/

// Handle ajax calls
const handleAJAX = async function (type, queryString) {
    try {
        const res = await fetch(`https://127.0.0.1:8086/api/${queryString}`, {
            method: type === 'list' ? 'GET' : 'POST'
        });

        const data = type === 'disconnect' ? undefined : await res.json();

        return data;
    } catch (err) {
        console.error(err);
    }
};

const createQueryString = function (type, data) {
    const query = [];

    for (const [key, value] of data) {
        if (type === 'connect') {
            query.push(`${key.replace('win_auth', 'winAuth')}=${value.replace('\\', '%5C')}`);
        }
        else {
            query.push(value);
        }
    }

    return `${type === 'connect' ? 'connect?' : 'kill/'}${query.join('&')}`;
};

// disable user name and password fields if using windows authentication
winAuthChk.addEventListener('change', function () {
    loginFields.forEach(field => {
        const disabled = this.checked ? true : false;
        field.disabled = disabled;
        field.required = !disabled;
        field.value = '';
    });
});

// handle form submit
document.addEventListener('submit', async function (e) {
    e.preventDefault();

    spinner.renderSpinner();

    const type = e.submitter.id.replace('_btn', '');
    if (type.includes('connect')) {
        connectBtn.disabled = type === 'connect' ? true : false;
        disconnectBtn.disabled = type === 'connect' ? false : true;

        killBtn.disabled = type === 'connect' ? false : true;
        listBtn.disabled = type === 'connect' ? false : true;

        createList.clear();
    }

    const data = type === 'connect' ? new FormData(connectForm, connectBtn) : new FormData(killForm, killBtn);

    const queryString = type === 'disconnect' ? 'disconnect' : createQueryString(type, data);
    handleAJAX(type, queryString)
        .then(() => {
            if (type === 'kill') {
                const row = document.getElementById(`spid${queryString.replace('kill/', '')}`);

                const classesToRemove = ['even', 'odd'];
                row.classList.remove(...classesToRemove);
                row.classList.add('dead');
            }
            spinner.clear();
        });
});

// handle list server processes
listBtn.addEventListener('click', function () {
    spinner.renderSpinner();

    handleAJAX('list', 'list')
        .then(data => {
            createList.render(data.data.recordsets);
            spinner.clear();
        });
});